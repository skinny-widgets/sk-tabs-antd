
import { SkTabsImpl }  from '../../sk-tabs/src/impl/sk-tabs-impl.js';

import { OPEN_AN, TITLE_AN } from "../../sk-tabs/src/sk-tabs.js";

import { DISABLED_AN } from "../../sk-core/src/sk-element.js";

export class AntdSkTabs extends SkTabsImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'tabs';
    }

    get tabsEl() {
        if (! this._tabsEl) {
            this._tabsEl = this.comp.el.querySelector('.ant-tabs');
        }
        return this._tabsEl;
    }

    set tabsEl(el) {
        this._tabsEl = el;
    }

    get titleBarEl() {
        if (! this._tabsTitleBarEl) {
            this._tabsTitleBarEl = this.tabsEl.querySelector('.ant-tabs-nav div');
        }
        return this._tabsTitleBarEl;
    }

    set titleBarEl(el) {
        this._tabsTitleBarEl = el;
    }

    get tabsContentEl() {
        if (! this._titleContentEl) {
            this._titleContentEl = this.tabsEl.querySelector('.ant-tabs-content');
        }
        return this._titleContentEl;
    }

    set tabsContentEl(el) {
        this._titleContentEl = el;
    }

    get inkBarEl() {
        if (! this._inkBarEl) {
            this._inkBarEl = this.tabsEl.querySelector('.ant-tabs-ink-bar-animated');
        }
        return this._inkBarEl;
    }

    set inkBarEl(el) {
        this._inkBarEl = el;
    }

    get subEls() {
        return [ 'tabsEl', 'titleBarEl', 'tabsContentEl', 'inkBarEl' ];
    }

    restoreState(state) {
        if (! this.comp.isTplSlotted) {
            this.clearAllElCache();
            this.tabsContentEl.innerHTML = '';
            this.titleBarEl.innerHTML = '';
            this.tabsContentEl.insertAdjacentHTML('beforeend', state.contentsState);
            this.renderTabs();
            this.bindTabSwitch();
            if (! this.titleBarEl.querySelector(`[${OPEN_AN}]`)) {
                this.updateTabs('tabs-1');
            }
        }
    }

    inkBarToTab(tab) {
        let activeTab = this.titleBarEl.querySelector(`[data-tab=${tab}]`);
        activeTab.classList.add('ant-tabs-tab-active');
        let rect = activeTab.getBoundingClientRect();
        let padding = window.getComputedStyle(activeTab, null).getPropertyValue('padding-left');
        this.inkBarEl.style.transform = `translate3d(${rect.left - parseFloat(padding)}px, 0px, 0px)`;
    }

    updateTabs(curTabId) {
        for (let tab of Object.keys(this.tabs)) {
            if (tab === curTabId) {
                let activeTabPane = this.tabs[tab];
                activeTabPane.style.display = 'block';
                activeTabPane.classList.add('ant-tabs-tabpane-active');
                this.inkBarToTab(tab);
            } else {
                this.tabs[tab].style.display = 'none';
                this.tabs[tab].classList.remove('ant-tabs-tabpane-active');
                this.titleBarEl.querySelector(`[data-tab=${tab}]`).classList.remove('ant-tabs-tab-active');
            }
        }
    }

    renderTabs(tabEls) {
        if (this.comp.isTplSlotted) {
            this.renderSlottedTabs(tabEls);
        } else {
            //this.tabsEl.insertAdjacentHTML('beforeend', this.contentsState || this.comp.contentsState);
            let tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
            let num = 1;
            this.tabs = {};
            for (let tab of tabs) {
                let isOpen = tab.hasAttribute(OPEN_AN);
                let title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
                this.titleBarEl.insertAdjacentHTML('beforeend', `
                    <div data-tab="tabs-${num}" aria-disabled="${tab.hasAttribute(DISABLED_AN) ? 'true' : 'false'}" aria-selected="true" class="ant-tabs-tab ${isOpen ? 'ant-tabs-tab-active' : ''} ${tab.hasAttribute(DISABLED_AN) ? 'ant-tabs-tab-disabled' : ''}"
                         role="tab" ${isOpen ? 'open' : ''}>${title}
                    </div>
                `);
                this.tabsContentEl.insertAdjacentHTML('beforeend', `
                    <div data-tab="tabs-${num}" aria-hidden="false" class="ant-tabs-tabpane ${isOpen ? 'ant-tabs-tabpane-active' : ''}" 
                        ${! isOpen ? 'style="display: none;"' : ''} role="tabpanel">
                        ${tab.outerHTML}
                    </div>
                `);
                if (isOpen) {
                    this.inkBarToTab(num);
                }
                this.removeEl(tab);
        
                this.tabsContentEl.querySelectorAll('div').forEach(tab => {
                    if (tab.getAttribute('data-tab') === 'tabs-' + num ) {
                        this.tabs['tabs-' + num] = tab;
                    }
                });
                num++;
            }
        }
    }

    bindTabSwitch() {
        this.titleBarEl.querySelectorAll('div').forEach(function(link) {
            link.onclick = function(event) {
                if (this.comp.hasAttribute(DISABLED_AN) 
                    || event.target.classList.contains('ant-tabs-tab-disabled')
                    || event.target.parentElement.classList.contains('ant-tabs-tab-disabled')) {
                    return false;
                }
                let tabId = event.target.getAttribute('data-tab');
                this.updateTabs(tabId);
            }.bind(this);
        }.bind(this));
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


    disable() {
        super.enable();
        this.titleBarEl.querySelectorAll('div[role=tab]').forEach((tab) => {
            tab.classList.add('ant-tabs-tab-disabled');
        });
    }

    enable() {
        super.disable();
        this.titleBarEl.querySelectorAll('div[role=tab]').forEach((tab) => {
            tab.classList.remove('ant-tabs-tab-disabled');
        });
    }
}
