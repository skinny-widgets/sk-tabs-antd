
import { SkSltabsImpl }  from '../../sk-tabs/src/impl/sk-sltabs-impl.js';

export class AntdSkSltabs extends SkSltabsImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'sltabs';
    }

}
