
import { SkTabImpl }  from '../../sk-tabs/src/impl/sk-tab-impl.js';

export class AntdSkTab extends SkTabImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'tab';
    }

}
